RPT shortener version 2 
                  
	 This command line tool shortens given rpt files. 
	  
	 Arguments: 
	 --help: displays help with a list of all arguments 
	  
	 --initialSetup: runs initial setup, creates a config file and the default output folder. 
	  
	 -i=<path>   the path (absolute or relative) to the input file 
	  
	 -o=<path>   the path (absolute or relative) to the output folder! 
	  
	 -d or -d=true toggles the preventing of doubles on 
	 -d=false toggles the preventing of doubles off 
	  
	 -s or -s=true toggles gathering statistics on 
	 -s=false toggles the gathering of statistics off 
	  
	 -saveConfig=<Path>   saves the new config to an xml file at the specified path 
	  
	 -a=<path>   the path (absolute or relative) to the file in which admin messages should be saved 
        
        
For help or more information contact me via gitlab.com/stanhope.