package model;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.nio.file.Path;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;


/**
 * A class that only exists to easily be able to write the stats to an XML file.  I should really write it myself so the config file has a more logical order to it but hey, this is the easy way to do it.
 */
@SuppressWarnings("unused") /*Supressing these because they're used by the xml writer but intellij doesn't realise that.*/
@XmlRootElement
public class Stats {

    private int linesProcessed = 0;
    private int linesDeleted = 0;
    private int doublesPrevented = 0;
    private int linesWritten = 0;

    private Map<String, Integer> linesRemoved;
    private Map<String, Integer> adminsNotified;

    private String timeTakenString;

    private double readFileSize;
    private double writeFileSize;

    Stats(int linesProcessed, int linesDeleted, int doublesPrevented, int linesWritten, Map<String, Integer> linesRemoved, Map<String, Integer> adminsNotified, LocalTime startTime, LocalTime endTime, File readFile, File writenFile, Path savePath) throws JAXBException {
        this.linesProcessed = linesProcessed;
        this.linesDeleted = linesDeleted;
        this.doublesPrevented = doublesPrevented;
        this.linesWritten = linesWritten;
        this.linesRemoved = linesRemoved;
        this.adminsNotified = adminsNotified;

        readFileSize = readFile.length();
        writeFileSize = writenFile.length();
        LocalTime timeTaken = endTime.minusNanos(startTime.toNanoOfDay());
        timeTakenString = timeTaken.format(DateTimeFormatter.ISO_LOCAL_TIME);

        writeToXML(savePath);
    }

    public Stats() {
    }

    /**
     * Method to write the config file to xml.  Called from the constructor and will write to the specified path.
     *
     * @throws JAXBException thrown when something went wrong with writing the config file.
     */
    private void writeToXML(Path savePath) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Stats.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(this, savePath.toFile());
    }

    public int getLinesProcessed() {
        return linesProcessed;
    }

    public void setLinesProcessed(int linesProcessed) {
        this.linesProcessed = linesProcessed;
    }

    public int getLinesDeleted() {
        return linesDeleted;
    }

    public void setLinesDeleted(int linesDeleted) {
        this.linesDeleted = linesDeleted;
    }

    public int getDoublesPrevented() {
        return doublesPrevented;
    }

    public void setDoublesPrevented(int doublesPrevented) {
        this.doublesPrevented = doublesPrevented;
    }

    public int getLinesWritten() {
        return linesWritten;
    }

    public void setLinesWritten(int linesWritten) {
        this.linesWritten = linesWritten;
    }

    public Map<String, Integer> getLinesRemoved() {
        return linesRemoved;
    }

    public void setLinesRemoved(Map<String, Integer> linesRemoved) {
        this.linesRemoved = linesRemoved;
    }

    public Map<String, Integer> getAdminsNotified() {
        return adminsNotified;
    }

    public void setAdminsNotified(Map<String, Integer> adminsNotified) {
        this.adminsNotified = adminsNotified;
    }

    public String getTimeTakenString() {
        return timeTakenString;
    }

    public void setTimeTakenString(String timeTakenString) {
        this.timeTakenString = timeTakenString;
    }

    public double getReadFileSize() {
        return readFileSize;
    }

    public void setReadFileSize(double readFileSize) {
        this.readFileSize = readFileSize;
    }

    public double getWriteFileSize() {
        return writeFileSize;
    }

    public void setWriteFileSize(double writeFileSize) {
        this.writeFileSize = writeFileSize;
    }
}
