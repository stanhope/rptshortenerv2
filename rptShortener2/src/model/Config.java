package model;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;

/**
 * This class contains all the configurations needed for the program.  Methods are provided to save this information to a config file.
 *
 * @author Gerd Goemans
 * @version 1.0
 */
@SuppressWarnings("WeakerAccess") /*Suppressing these warnings because I have to make them for the XML writer I'm using*/
@XmlRootElement
public class Config {

    private boolean removeDoubles;
    private boolean gatherStatistics;
    private String[] removeKeywords;
    private String[] notifyAdminsKeywords;

    public Config() {
        removeKeywords = defaultRemoveKeywords();
        notifyAdminsKeywords = defaultNotifyAdminsKeywords();
        removeDoubles = true;
        gatherStatistics = false;
    }


    public boolean isRemoveDoubles() {
        return removeDoubles;
    }

    public void setRemoveDoubles(boolean removeDoubles) {
        this.removeDoubles = removeDoubles;
    }

    public boolean isGatherStatistics() {
        return gatherStatistics;
    }

    public void setGatherStatistics(boolean gatherStatistics) {
        this.gatherStatistics = gatherStatistics;
    }

    public String[] getRemoveKeywords() {
        if (removeKeywords == null || removeKeywords.length == 0) {
            removeKeywords = defaultRemoveKeywords();
        }
        return removeKeywords;
    }

    public void setRemoveKeywords(String[] removeKeywords) {
        this.removeKeywords = removeKeywords;
        for (int i = 0; i < this.removeKeywords.length; i++) {
            this.removeKeywords[i] = this.removeKeywords[i].toLowerCase();
        }

    }

    public String[] getNotifyAdminsKeywords() {
        if (notifyAdminsKeywords == null || notifyAdminsKeywords.length == 0) {
            notifyAdminsKeywords = defaultNotifyAdminsKeywords();
        }
        return notifyAdminsKeywords;
    }

    public void setNotifyAdminsKeywords(String[] notifyAdminsKeywords) {
        this.notifyAdminsKeywords = notifyAdminsKeywords;

        for (int i = 0; i < this.notifyAdminsKeywords.length; i++) {
            this.notifyAdminsKeywords[i] = this.notifyAdminsKeywords[i].toLowerCase();
        }
    }

    /**
     * Same method as writeToXML(String path) but with a default value for path
     *
     * @throws JAXBException in case object could not be parsed
     */
    public void writeToXML() throws JAXBException {
        writeToXML("rptShortenerConfig.xml");
    }

    /**
     * Method to write the config file to a given location in xml format
     *
     * @param path The path to save the file to
     * @throws JAXBException in case object could not be parsed
     */
    public void writeToXML(String path) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Config.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(this, new File(path));
    }


    /**
     * Same method as readFromXML(String path) but with a default value for path
     *
     * @throws JAXBException in case the object could not be parsed
     */
    public void readFromXML() throws JAXBException {
        readFromXML("rptShortenerConfig.xml");
    }


    /**
     * Reads a config file and puts the values in the current Config class.
     *
     * @param path the path to read the file from
     * @throws JAXBException in case the object could not be parsed
     */
    public void readFromXML(String path) throws JAXBException {
        File f = new File(path);

        if (!f.exists()) {
            System.out.println("Config does not exist.  Using default values.");
            return;
        }

        JAXBContext jc = JAXBContext.newInstance(Config.class);
        Unmarshaller u = jc.createUnmarshaller();

        Config config = (Config) u.unmarshal(f);

        this.setGatherStatistics(config.isGatherStatistics());
        this.setNotifyAdminsKeywords(config.getNotifyAdminsKeywords());
        this.setRemoveDoubles(config.isRemoveDoubles());
        this.setRemoveKeywords(config.getRemoveKeywords());
    }

    public static String[] defaultRemoveKeywords() {
        return new String[]{"strange", "duplicate", "invalid", "unknown animation", "geometry", "not found",
                "unable to connect", "network", "nonnetwork", "beserver", "player without identity",
                "performance warning", "some of magazines", "no speaker", "getting out while",
                "message not sent", "netserver", "error compiling", "unaccessible",
                "does not match to this weapon", "updating base class", "deinitialized shape",
                "message was repeated in last", "dimensions in class", "addons\\",
                "\\Steam\\steamapps\\common\\", "movestype", "===", "---", "postinit finished",
                "preinit started", "number of roles", "epe manager release", "cannot create object",
                "unknown action", "preinit finished", "unable to get word", "number of actors in scene after release"};
    }

    public static String[] defaultNotifyAdminsKeywords() {
        return new String[]{"hacker", "tk script", "zeus"};
    }

}
