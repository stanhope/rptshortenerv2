package model;

/**
 * A pretty useless class that prints the text when the --help argument is passed.  Only used it because I couldn't get a freaking filereader to work when I put it in a .txt file.  So it's become this because it just works.
 */
public class PrintHelpHelper {

    public PrintHelpHelper() {
        System.out.println("RPT shortener version 2\n" +
                "\n" +
                "This command line tool shortens given rpt files.\n" +
                "\n" +
                "Arguments:\n" +
                "\n" +
                "--help: displays help with a list of all arguments\n" +
                "\n" +
                "--initialSetup: runs initial setup, creates a config file and the default output folder.\n" +
                "\n" +
                "-i=\"<path>\" the path (absolute or relative) to the input file\n" +
                "\n" +
                "-o=\"<path>\" the path (absolute or relative) to the output folder!\n" +
                "\n" +
                "-d or -d=true toggles the preventing of doubles on\n" +
                "-d=false toggles the preventing of doubles off\n" +
                "\n" +
                "-s or -s=true toggles gathering statistics on\n" +
                "-s=false toggles the gathering of statistics off\n" +
                "\n" +
                "-saveConfig=\"<Path>\" saves the new config to an xml file at the specified path\n" +
                "-xml=\"<Path>\" path to the config file to be used.  Config entries will be overwritten by other passed params but not written to that file unless save argument is passed as well.\n" +
                "\n" +
                "-a=\"<path>\" the path (absolute or relative) to the file in which admin messages should be saved.  If file already exists it will be deleted and recreated.  If file does not exist it will be created."
        );
    }
}
