package model;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The class that actually shortens the RPT.  It uses an instance of {@link Config} to get all the needed information for its workings.
 * Depending on the settings in the {@link Config} it will also create an instance of {@link Stats} to write information on its workings to an XML file.
 *
 * @author Gerd Goemans
 * @version 1.0
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
public class Shortener {

    private Path filePath;
    private Path newFilePath;
    private Path adminPath;

    private boolean gatherStats;
    private boolean removeDoubles;

    private String[] removeKeyWords;
    private String[] adminNotifyWords;

    private List<String> doublesList;
    private Map<String, Integer> adminsNotifiedStats;
    private Map<String, Integer> linesRemovedStats;

    /**
     * Constructor with 3 params
     *
     * @param config      an instance of the {@link Config} class to grab the passed params from the user
     * @param inputFile   the file to read from
     * @param newFilePath the path to the folder in which to write
     */
    public Shortener(Config config, Path inputFile, Path newFilePath) {
        setFilePath(inputFile);
        setNewFilePath(newFilePath);
        setAdminNotifyWords(config.getNotifyAdminsKeywords());
        setRemoveKeyWords(config.getRemoveKeywords());
        setGatherStats(config.isGatherStatistics());
        setRemoveDoubles(config.isRemoveDoubles());

        if (removeDoubles) doublesList = new ArrayList<>();
        if (gatherStats) {
            doublesList = new ArrayList<>();
            adminsNotifiedStats = new TreeMap<>();
            linesRemovedStats = new TreeMap<>();
        }
    }

    /**
     * A static method to remove the date tag from a line.  (Spoiler alert it's just a fancy thing that removes the first 20 characters from the line because I couldn't be bothered to actually write a function to detect whether or not the line has a date and in which format this date is.)
     *
     * @param line the line to remove the date tag from.
     * @return String, the line with the date tag removed form it.
     */
    private static String removeDate(String line) {
        if (line.length() > 20) {
            return line.substring(20);
        } else {
            return line;
        }
    }

    /**
     * A method that just calls the basic getter so it creates the path if it doesn't exist yet and prints the basic config xml to the default file.
     */
    public void runSetup() {
        getNewFilePath();
        try {
            new Config().writeToJSON();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * The main method of this class.  It loops over all the lines in a file and checks if it contains one of the admin notify keywords or one of the remove keywords.  More documentation is provided by inline comments.
     */
    public void run() {
        /*Set up some basic vars for the stats class*/
        int linesProcessed = 0;
        int linesDeleted = 0;
        int doublesPrevented = 0;
        int linesWritten = 0;
        LocalTime startTime = null;
        LocalTime endTime = null;

        /*Get the filepath convert it into a file and grab the filename from it.*/
        File input = getFilePath().toFile();
        String inputFileName = getFilePath().toString();
        if (inputFileName.contains("/")) {
            inputFileName = inputFileName.substring(inputFileName.lastIndexOf("/"));
        } else if (inputFileName.contains("\\")) {
            inputFileName = inputFileName.substring(inputFileName.lastIndexOf("\\"));
        }

        /*get the outputpath, add the originnal filename to the end of it and add the suffix _short to it.*/
        Path outPutFile = Paths.get(getNewFilePath() + inputFileName.replace(".rpt", "_short.rpt"));
        File outputFile = outPutFile.toFile();

        /*Create the writer that will write to the admin file if needed*/
        BufferedWriter adminWriter = null;
        if (adminPath != null) {
            try {
                adminWriter = new BufferedWriter(new FileWriter(adminPath.toFile()));
            } catch (IOException e) {
                System.err.println("Could not create admin writer.  No admin logs written.");
            }
        }

        /*Set up some more things for the stats class if needed*/
        if (gatherStats) {
            for (String removeKeyWord : removeKeyWords) {
                linesRemovedStats.putIfAbsent(removeKeyWord, 0);
            }
            for (String adminNotifyWord : adminNotifyWords) {
                adminsNotifiedStats.putIfAbsent(adminNotifyWord, 0);
            }
        }

        /*Create the file reader and writer*/
        try (
                BufferedReader reader = new BufferedReader(new FileReader(input));
                BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))
        ) {

            System.out.println("\nStarting ...");
            if (gatherStats) startTime = LocalTime.now();

            /*Start by reading the first line*/
            String line = reader.readLine();

            /*Loop over all the lines in the file*/
            while (line != null) {

                if (gatherStats) linesProcessed++;

                /*Make it lowercase*/
                line = line.toLowerCase();

                /*Run the admin check if needed*/
                if (adminWriter != null) {
                    for (String toCheck : adminNotifyWords) {
                        /*check if the line contains the keyword*/
                        if (line.contains(toCheck)) {
                            /*If it does, write it to the file*/
                            adminWriter.write(line);
                            adminWriter.write("\n");
                            adminWriter.flush();
                            if (gatherStats) {
                                /*if we're gathering stats track how often we delete a line containing a certain string*/
                                int count = adminsNotifiedStats.getOrDefault(toCheck, 1);
                                adminsNotifiedStats.put(toCheck, count + 1);
                            }
                            break;
                        }
                    }
                }

                /*check if the line contains one of the keywords*/
                for (String toCheckString : removeKeyWords) {
                    if (line.contains(toCheckString)) {
                        /*if it contains the line set the line to null and break out of this for*/
                        if (gatherStats) {
                            /*if we're gathering stats track how often we delete a line containing a certain string*/
                            int count = linesRemovedStats.getOrDefault(toCheckString, 1);
                            linesRemovedStats.put(toCheckString, count + 1);

                            linesDeleted++;
                        }

                        line = null;
                        break;
                    }
                }


                /*do check if line is not null and doubles should be prevented*/
                if (line != null && removeDoubles) {
                    /*remove the timestamp from the line*/
                    String lineCopy = line;
                    lineCopy = removeDate(lineCopy);

                    /*check if we already have this line*/
                    if (doublesList.contains(lineCopy)) {
                        /*It's already in the list so remove it*/
                        line = null;
                        if (gatherStats) doublesPrevented++;
                    } else {
                        /*it's not in the list, add it*/
                        doublesList.add(lineCopy);
                    }
                }

                /*if line wasn't removed write it*/
                if (line != null) {
                    writer.write(line);
                    writer.write("\n");
                    if (gatherStats) linesWritten++;
                }

                /*read the next line*/
                line = reader.readLine();
            }

            System.out.println("Finalizing ...");
            writer.flush();

            if (gatherStats) endTime = LocalTime.now();

            /*Send everything to the stats class for writing.*/
            if (gatherStats) {
                if (!removeDoubles) doublesPrevented = -1;
                Path statsOut = Paths.get(getNewFilePath().toString() + inputFileName.replace(".rpt", "_stats.rpt"));
                //noinspection ConstantConditions
                new Stats(linesProcessed, linesDeleted, doublesPrevented, linesWritten, linesRemovedStats, adminsNotifiedStats, startTime, endTime, filePath.toFile(), outPutFile.toFile(), statsOut);
            }

        } catch (Exception e) {
            System.err.println("Fatal error: ");
            System.err.println(e.getMessage());
            System.exit(1);
        } finally {
            /*Close the admin writer if needed*/
            if (adminWriter != null) {
                try {
                    adminWriter.flush();
                    adminWriter.close();
                } catch (IOException e) {/*ignore*/}
            }
        }
    }

    private Path getFilePath() {
        if (!this.filePath.toFile().exists()) {
            filePath.toFile().mkdirs();
        }
        return filePath;
    }

    private void setFilePath(Path filePath) {
        this.filePath = filePath;
    }

    private Path getNewFilePath() {
        if (!this.newFilePath.toFile().exists()) {
            newFilePath.toFile().mkdirs();
        }
        return newFilePath;
    }

    private void setNewFilePath(Path newFilePath) {
        this.newFilePath = newFilePath;
    }

    private void setRemoveKeyWords(String[] removeKeyWords) {
        this.removeKeyWords = removeKeyWords;
    }

    private void setAdminNotifyWords(String[] adminNotifyWords) {
        this.adminNotifyWords = adminNotifyWords;
    }

    private void setGatherStats(boolean gatherStats) {
        this.gatherStats = gatherStats;
    }

    private void setRemoveDoubles(boolean removeDoubles) {
        this.removeDoubles = removeDoubles;
    }

    public void setAdminPath(Path adminPath) {
        this.adminPath = adminPath;
    }
}
