package model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;

/**
 * This class contains all the configurations needed for the program.  Methods are provided to save this information to a config file.
 *
 * @author Gerd Goemans
 * @version 1.0
 */
@SuppressWarnings("WeakerAccess") /*Suppressing these warnings because I have to make them for the XML writer I'm using*/
public class Config {
    //todo: fix documentation
    private boolean removeDoubles;
    private boolean gatherStatistics;
    private String[] removeKeywords;
    private String[] notifyAdminsKeywords;

    public Config() {
        removeKeywords = defaultRemoveKeywords();
        notifyAdminsKeywords = defaultNotifyAdminsKeywords();
        removeDoubles = true;
        gatherStatistics = false;
    }


    public boolean isRemoveDoubles() {
        return removeDoubles;
    }

    public void setRemoveDoubles(boolean removeDoubles) {
        this.removeDoubles = removeDoubles;
    }

    public boolean isGatherStatistics() {
        return gatherStatistics;
    }

    public void setGatherStatistics(boolean gatherStatistics) {
        this.gatherStatistics = gatherStatistics;
    }

    public String[] getRemoveKeywords() {
        if (removeKeywords == null || removeKeywords.length == 0) {
            removeKeywords = defaultRemoveKeywords();
        }
        return removeKeywords;
    }

    public void setRemoveKeywords(String[] removeKeywords) {
        this.removeKeywords = removeKeywords;
        for (int i = 0; i < this.removeKeywords.length; i++) {
            this.removeKeywords[i] = this.removeKeywords[i].toLowerCase();
        }

    }

    public String[] getNotifyAdminsKeywords() {
        if (notifyAdminsKeywords == null || notifyAdminsKeywords.length == 0) {
            notifyAdminsKeywords = defaultNotifyAdminsKeywords();
        }
        return notifyAdminsKeywords;
    }

    public void setNotifyAdminsKeywords(String[] notifyAdminsKeywords) {
        this.notifyAdminsKeywords = notifyAdminsKeywords;

        for (int i = 0; i < this.notifyAdminsKeywords.length; i++) {
            this.notifyAdminsKeywords[i] = this.notifyAdminsKeywords[i].toLowerCase();
        }
    }

    /**
     * Same method as writeToJSON(String path) but with a default value for path
     */
    public void writeToJSON() throws IOException {
        writeToJSON("rptShortenerConfig.json");
    }

    /**
     * Method to write the config file to a given location in xml format
     *
     * @param path The path to save the file to
     */
    public void writeToJSON(String path) throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        String jsonString = gson.toJson(this);
        FileWriter jsonwriter = new FileWriter(new File(path));
        jsonwriter.write(jsonString);
        jsonwriter.flush();
        jsonwriter.close();
    }


    /**
     * Same method as readFromJSON(String path) but with a default value for path
     */
    public void readFromJSON() throws FileNotFoundException {
        readFromJSON("rptShortenerConfig.json");
    }


    /**
     * Reads a config file and puts the values in the current model.Config class.
     *
     * @param path the path to read the file from
     */
    public void readFromJSON(String path) throws FileNotFoundException {
        Config config = null;
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        BufferedReader data = new BufferedReader(new FileReader(new File(path)));
        config = gson.fromJson(data, Config.class);

        this.setGatherStatistics(config.isGatherStatistics());
        this.setNotifyAdminsKeywords(config.getNotifyAdminsKeywords());
        this.setRemoveDoubles(config.isRemoveDoubles());
        this.setRemoveKeywords(config.getRemoveKeywords());
    }

    public static String[] defaultRemoveKeywords() {
        return new String[]{"strange", "duplicate", "invalid", "unknown animation", "geometry", "not found",
                "unable to connect", "network", "nonnetwork", "beserver", "player without identity",
                "performance warning", "some of magazines", "no speaker", "getting out while",
                "message not sent", "netserver", "error compiling", "unaccessible",
                "does not match to this weapon", "updating base class", "deinitialized shape",
                "message was repeated in last", "dimensions in class", "addons\\",
                "\\Steam\\steamapps\\common\\", "movestype", "===", "---", "postinit finished",
                "preinit started", "number of roles", "epe manager release", "cannot create object",
                "unknown action", "preinit finished", "unable to get word", "number of actors in scene after release"};
    }

    public static String[] defaultNotifyAdminsKeywords() {
        return new String[]{"hacker", "tk script", "zeus"};
    }

}
