import model.Config;
import model.PrintHelpHelper;
import model.Shortener;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Main {

    private static String inputPath = null;
    private static String outputPath = "";
    private static String configPath = null;
    private static String configSavePath = null;
    private static String adminNotifyPath = null;
    private static boolean preventDouble = true;
    private static boolean gatherStats = false;
    private static boolean saveConfig = false;
    private static Path inputFile = null;
    private static Path outPath = null;
    private static Path adminPath = null;

    public static void main(String[] args) {
        List<String> argsList = Arrays.asList(args);

        if (argsList.contains("--help")) {
            new PrintHelpHelper();
            System.exit(0);
        } else if (argsList.contains("--initialSetup")) {
            runSetup();
        } else {
            checkArgumentsList(argsList);
            runInputPathCheck(argsList.size());

            System.out.println("Setting up ...");
            Config config = setConfigUp();

            /*Main function*/
            launchShortener(config);

            /*Wait for the program to finish*/
            System.out.println("\nDone");
        }
    }

    private static void launchShortener(Config config) {
        Shortener shortener = new Shortener(config, inputFile, outPath);
        if (adminPath != null) shortener.setAdminPath(adminPath);
        shortener.run();
    }

    private static void runInputPathCheck(int argsListSize) {
        if (inputPath == null) {
            if (argsListSize > 0) {
                System.err.println("No inputPath or other valid params passed.  Pass --help for more info, exiting");
                System.exit(1);
            } else {
                System.err.println("Cowardly refused to do nothing");
                System.err.println("Pass --help for more information");
                System.exit(1);
            }
        }
    }

    private static Config setConfigUp() {
        Config config = new Config();
        try {
            if (configPath != null) {
                config.readFromJSON(configPath);
            } else {
                config.readFromJSON();
            }
        } catch (Exception e) {
            System.err.println("Could not parse config file: ");
            System.err.println(e.getMessage());
            System.err.println("Proceeding with default values.");
        }

        inputFile = Paths.get(inputPath);
        if (!inputFile.toFile().isFile()) {
            System.err.println("Given inputFile is not a file, exiting");
            System.exit(1);
        }

        outPath = Paths.get(outputPath);
        if (!outputPath.equals("")) {
            if (!outPath.toFile().isDirectory()) {
                System.err.println("Given outputfolder is not a folder, exiting");
                System.exit(1);
            }
        }

        if (adminNotifyPath != null) {
            adminPath = Paths.get(adminNotifyPath);
            try {
                Files.deleteIfExists(adminPath);
                Files.createFile(adminPath);
                if (!adminPath.toFile().isFile()) {
                    throw new IOException();
                }
            } catch (IOException e) {
                System.err.println("Something went wrong while setting up the admin log file.  No admin logs have been created.");
                adminPath = null;
            }
        }

        config.setGatherStatistics(gatherStats);
        config.setRemoveDoubles(preventDouble);
        if (saveConfig) {
            try {
                if (configSavePath.equals("")) {
                    config.writeToJSON();
                } else {
                    config.writeToJSON(configSavePath);
                }
            } catch (Exception e) {
                System.err.println("Could not save config to json:");
                System.err.println(e.getMessage());
                System.err.println("Non-fatal, continuing program");
            }
        }
        return config;
    }

    private static void checkArgumentsList(List<String> argsList) {
        for (String arg : argsList) {
            if (arg.startsWith("-i=")) {
                //trim the -i= from the line
                inputPath = arg.substring(3);
                continue;
            }

            if (arg.startsWith("-a=")) {
                //trim the -i= from the line
                adminNotifyPath = arg.substring(3);
                continue;
            }

            if (arg.startsWith("-o=")) {
                //trim the -i= from the line
                outputPath = arg.substring(3);
                continue;
            }

            if (arg.startsWith("-json=")) {
                configPath = arg.substring(6);
            }

            if (arg.startsWith("-saveConfig")) {
                saveConfig = true;
                configSavePath = arg.substring(12);

                continue;
            }

            if (arg.contains("-d")) {
                if (arg.equals("-d=true") || arg.equals("-d")) {
                    preventDouble = true;
                    continue;
                } else if (arg.equals("-d=false")) {
                    preventDouble = false;
                    continue;
                }
            }

            if (arg.contains("-s")) {
                if (arg.equals("-s=true") || arg.equals("-s")) {
                    gatherStats = true;
                } else if (arg.equals("-s=false")) {
                    gatherStats = false;
                }
            }
        }
    }

    private static void runSetup() {
        try {
            System.out.println("Running initial setup");
            Config config = new Config();
            config.writeToJSON();
            Shortener shortener = new Shortener(config, Paths.get(""), Paths.get(""));
            shortener.runSetup();
            System.out.println("Initial setup done");

            System.exit(0);
        } catch (Exception e) {
            System.err.println("Fatal system error, terminating:");
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }
}
